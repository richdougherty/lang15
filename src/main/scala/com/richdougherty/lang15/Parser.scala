package com.richdougherty.lang15

import org.parboiled.scala._
import org.parboiled.scala.{ Parser => ParboiledParser }

class Parser extends ParboiledParser {
  def expr: Rule1[Val] = rule { (str | dqstr | sexpr) }
  def str: Rule1[StrVal] = rule {
    oneOrMore("a" - "z" | "A" - "Z" | "0" - "9" | "*" | "/" | "+" | "-" | "<") ~> StrVal
  }
  def dqstr: Rule1[StrVal] = rule { "\"" ~ zeroOrMore("a" - "z" | "A" - "Z" | "0" - "9" | " " | "!") ~> StrVal ~ "\""  }
  def sexpr: Rule1[RecVal] = rule { "(" ~ str ~ zeroOrMore(ws1 ~ expr) ~ ")" ~~> {
    case (name, positional) => RecVal(
      "name" -> name,
      "positional" -> ArrVal(positional: _*)
    )
  }}
  def program: Rule1[Val] = rule { ws0 ~ expr ~ ws0 ~ EOI }
  def ws0: Rule0 = rule { zeroOrMore(wsChar) }
  def ws1: Rule0 = rule { oneOrMore(wsChar) }
  def wsChar: Rule0 = rule { anyOf(" \t\n") }

  def parse(input: String): Option[Val] = {
    val runner = ReportingParseRunner(program)
    val parsingResult = runner.run(input)
    parsingResult.result
  }
}
package com.richdougherty.lang15

object Main {

  def main(args: Array[String]): Unit = {
    val parser = new Parser()
    val source = """
    |(println (literal "Hello world!"))
    |""".stripMargin('|')
    val result = Interpreter.run(source)
    println(result)
  }

}
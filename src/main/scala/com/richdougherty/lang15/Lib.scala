package com.richdougherty.lang15

import scala.collection.mutable.Buffer

object Lib {

  def fun(name: String)(f: (Args, Ctx) => Step): (String, FunVal) = {
    name -> FunVal(new Fun {
      override def toString = s"<$name>"
      def apply(args: Args, ctx: Ctx): Step = f(args, ctx)
    })
  }

  object Eval extends Fun {
    override def toString = "<eval>"
    def apply(args: Args, ctx: Ctx): Step = {

      val positionalExprs = args.positional

      var evaluatedPositionals = Buffer[Val]()

      // Step to evaluate expression i
      def step(i: Int): Step = {
        if (i < positionalExprs.a.length) {

          val expr = Expr(positionalExprs.a(i).asInstanceOf[RecVal])

          Step(
            f = ctx.env.lookup(expr.name) match {
              case Some(f: FunVal) => f
              case Some(invalid) => LangException.error(s"Expression evaluator not a function: ${expr.name}: ${invalid}") 
              case None => LangException.error(s"Expression evaluator not defined: ${expr.name}")
            },
            args = {
              val funArgsRec: RecVal = RecVal()
              funArgsRec += ("positional" -> expr.positional.getOrElse(ArrVal()))
              Args(funArgsRec)
            },
            ctx = ctx.withK("express", stepk(i))
          )
        } else {
          Step(
            ctx.k("express"),
            Args(RecVal(
              "positional" -> ArrVal(evaluatedPositionals)
            )),
            ctx // FIXME: Update context with new env, return continuation, etc
          )
        }
      }

      // Continuation receiving result of evaluating expr i
      def stepk(i: Int): Fun = {
        assert(i < positionalExprs.a.length)
        val expr = positionalExprs.a(i)
        new Fun {
          override def toString = s"<eval:store-expr-$i>"
          def apply(args: Args, ctx: Ctx) = {
            val evaluatedValue: Val = args.extractPositional("Expected array with single element") {
              case Seq(v) => v
            }
            evaluatedPositionals += evaluatedValue
            step(i+1)
          }
        }
      }

      step(0)
    }
  }

  def lam(name: String)(f: (Args, Ctx) => Step): (String, FunVal) = {
    name -> FunVal(new Fun {
      override def toString = s"<$name>"
      def apply(args: Args, ctx: Ctx): Step = {
        val callerCtx = ctx

        val fk = new Fun {
          override def toString = s"<$name:body>"
          def apply(args: Args, ctx: Ctx) = {
            f(args, callerCtx) // evaluated args
          }
        }

        Step(
          f = FunVal(Eval),
          args = args,
          ctx = ctx.withK("express", fk)
        )
      }
    })
  }


  object Match extends Fun {
    override def toString = "<match>"

    // called with positional=ArrVal(PATTERN, VALUE)
    // looks up pattern, calls with value
    def apply(args: Args, ctx: Ctx): Step = {

      val (patternExpr, value) = args.extractPositional("Expected pattern and value") {
        case Seq(p: RecVal, v: Val) => (Expr(p), v)
      }

      val patternF: FunVal = ctx.env.lookup(patternExpr.name) match {
        case Some(f: FunVal) => f
        case Some(invalid) => LangException.error(s"Pattern not a function: ${patternExpr.name}: ${invalid}") 
        case None => LangException.error(s"Unknown pattern: ${patternExpr.name}")
      }

      val patternArgs = Args.positional(
        patternExpr.positional.getOrElse(ArrVal()),
        value
      )

      Step(patternF, patternArgs, ctx)

    }
  }

  def arithOp(name: String)(f: (Int, Int) => Int): (String, FunVal) = {
    lam(name) { (args: Args, ctx: Ctx) =>
      val result = args.positional.a.foldLeft[Option[IntVal]](None) {
        case (None, IntVal(x)) => Some(IntVal(x))
        case (Some(IntVal(acc)), IntVal(x)) => Some(IntVal(f(acc, x)))
        case (_, invalid) => LangException.error(s"Invalid argument for $name: $invalid")
      }.getOrElse(LangException.error(s"Missing arguments for $name"))
      Step(
        ctx.k("express"),
        Args(RecVal("positional" -> ArrVal(result))),
        ctx
      )
    }
  }

  def patt(name: String)(f: (ArrVal, Val, Ctx) => Step): (String, FunVal) = {
    name -> FunVal(new Fun {
      override def toString = s"<$name>"
      def apply(args: Args, ctx: Ctx): Step = {
        val (staticArgs, v) = args.extractPositional("Expected static args and a value") {
          case Seq(s: ArrVal, v: Val) => (s, v)
        }
        f(staticArgs, v, ctx)
      }
    })
  }

  def stdEnv(external: External): Env = {
    Env(RecVal(

      // EVAL //

      "eval" -> FunVal(Eval),

      // MATCH //

      "match" -> FunVal(Match),

      // EXIT //

      "exit" -> FunVal(Step.Exit),

      // VAU //

      fun("vau") { (args: Args, ctx: Ctx) =>
        val (argsPatt, envPatt, bodyExpr) = args.extractPositional("Expected args expression pattern, env pattern and a body") {
          case Seq(a: RecVal, e: RecVal, v: RecVal) => (a, e, v)
        }

        val vau = new Fun {
          override def toString = "<lambda>"
          def apply(args: Args, ctx: Ctx) = {
            val argExprs: Seq[Val] = args.extractPositional("Expected zero or more expressions") {
              case argExprs => argExprs
            }
            
            val envRec = ctx.env.rec

            val lastExpressK = ctx.k("express").f
            val vauCtx = ctx.withChildEnv

            val envMatchK = new Fun {
              override def toString = s"<vau:env-match>"
              def apply(args: Args, ctx: Ctx) = {
                val callerEnv = ctx.env
                
                val bodyK = new Fun {
                  override def toString = s"<vau:env-match>"
                  def apply(args: Args, ctx: Ctx) = {
                    Step(
                      FunVal(Eval),
                      Args.positional(bodyExpr),
                      ctx = vauCtx.withK("express", lastExpressK)
                    )
                  }
                }
                Step(
                  FunVal(Match),
                  args = Args.positional(envPatt, callerEnv.rec),
                  ctx = vauCtx.withK("express", bodyK)
                )
              }
            }

            Step(
              f = FunVal(Match),
              args = Args.positional(argsPatt, ArrVal(argExprs: _*)),
              ctx = vauCtx.withK("express", envMatchK)
            )
          }
        }

        // Return the constructed vau
        Step(
          f = ctx.k("express"),
          args = Args.positional(FunVal(vau)),
          ctx
        )
      },

      // QUOTE //

      fun("quote") { (args: Args, ctx: Ctx) =>
        Step(ctx.k("express"), args, ctx)
      },

      // LAMBDA //

      fun("lambda") { (args: Args, ctx: Ctx) =>
        val (patt, bodyExpr) = args.extractPositional("Expected args pattern and body") {
          case Seq(p: RecVal, v: RecVal) => (p, v)
        }

        val lambda = new Fun {
          override def toString = "<lambda>"
          def apply(args: Args, ctx: Ctx) = {
            val argExprs: Seq[Val] = args.extractPositional("Expected zero or more arguments") {
              case argExprs => argExprs
            }

            val lastExpressK = ctx.k("express").f
            val lambdaCtx = ctx.withChildEnv

            val pattK = new Fun {
              override def toString = s"<lambda:patt>"
              def apply(args: Args, ctx: Ctx) = {
                val argValues = args.positional

                val bodyK = new Fun {
                  override def toString = s"<lambda:body>"
                  def apply(args: Args, ctx: Ctx) = {
                    Step(
                      FunVal(Eval),
                      args = Args.positional(bodyExpr),
                      ctx = lambdaCtx.withK("express", lastExpressK)
                    )
                  }
                }

                Step(
                  f = FunVal(Match),
                  args = Args.positional(patt, argValues),
                  ctx = lambdaCtx.withK("express", bodyK)
                )

              }
            }

            // TODO: Add 'return' continuation
            Step(
              f = FunVal(Eval),
              args = Args.positional(argExprs: _*),
              ctx = ctx.withK("express", pattK)
            )
          }
        }

        Step(
          f = ctx.k("express"),
          args = Args.positional(FunVal(lambda)),
          ctx
        )
      },

      // STR //

      fun("str") { (args: Args, ctx: Ctx) =>
        val v: StrVal = args.extractPositional("Expected single string argument") {
          case Seq(v: StrVal) => v
        }
        Step(
          ctx.k("express"),
          Args.positional(v),
          ctx
        )
      },

      // INT //

      fun("int") { (args: Args, ctx: Ctx) =>
        val unparsed: String = args.extractPositional("Expected single string argument") {
          case Seq(StrVal(s)) => s
        }
        val v: IntVal = try {
          IntVal(Integer.parseInt(unparsed, 10))
        } catch {
          case nfe: NumberFormatException =>
            LangException.error(s"int expression format wrong: $unparsed")
        }
        Step(
          ctx.k("express"),
          Args.positional(v),
          ctx
        )
      },

      // ARR //

      lam("arr") { (args: Args, ctx: Ctx) =>
        val v: ArrVal = ArrVal(args.positional.a: _*)
        Step(
          ctx.k("express"),
          Args.positional(v),
          ctx
        )
      },

      // BLOCK //

      fun("block") { (args: Args, ctx: Ctx) =>
        val exprs: Seq[Val] = args.extractPositional("Expected zero or more expressions") {
          case s => s
        }

        val blockCtx = ctx.withChildEnv

        // Step to evaluate expression i
        def step(i: Int): Step = {
          assert(i < exprs.length)
          Step(
            f = FunVal(Eval),
            args = Args.positional(exprs(i)),
            ctx = blockCtx.withK("express", express(i))
          )
        }

        // What happens after expression i
        def express(i: Int): Fun = {
          if (i + 1 < exprs.length) {
            new Fun {
              override def toString = s"<block:express-$i>"
              def apply(args: Args, ctx: Ctx) = step(i+1)
            }
          } else {
            ctx.k("express").f
          }
        }

        // Either evaluate the first expression or express "unit"
        if (exprs.isEmpty) {
          Step(
            ctx.k("express"),
            Args.positional((StrVal("unit"))),
            ctx
          )
        } else {
          step(0)
        }
      },

      // LOOKUP //

      fun("lookup") { (args: Args, ctx: Ctx) =>
        val name: String = args.extractPositional("Expected single string argument") {
          case Seq(StrVal(s)) => s
        }

        val v = ctx.env.lookup(name) match {
          case Some(v) => v
          case None => LangException.error(s"Lookup didn't find: ${name}")
        }

        Step(
          f = ctx.k("express"),
          args = Args.positional(v),
          ctx = ctx
        )
      },

      // LET //

      fun("let") { (args: Args, ctx: Ctx) =>

        val (pattExpr, valueExpr) = args.extractPositional("Expected pattern and value arguments") {
          case Seq(p: RecVal, v) => (p, v)
        }

        val origCtx = ctx

        val letK = new Fun {
          override def toString = s"<let:evaled>"
          def apply(args: Args, ctx: Ctx) = {
            Step(
              f = FunVal(Match),
              args = Args.positional(pattExpr, args.positional),
              ctx = origCtx
            )
          }
        }

        Step(
          f = FunVal(Eval),
          args = Args.positional(valueExpr),
          ctx = ctx.withK("express", letK)
        )
      },

      // BIND-PATT //

      patt("bind-patt") { (staticArgs: ArrVal, v: Val, ctx: Ctx) =>

        val name: String = staticArgs match {
          case ArrVal(Seq(StrVal(name))) => name
          case invalid => LangException.error(s"bind-patt patterns take one child pattern: $invalid")
        }

        ctx.env.rec += (name -> v)

        Step(
          f = ctx.k("express"),
          args = Args.positional(StrVal("unit")),
          ctx = ctx
        )
      },

      // SINGLE-PATT //

      patt("single-patt") { (staticArgs: ArrVal, v: Val, ctx: Ctx) =>

        val childPatt: Expr = staticArgs match {
          case ArrVal(Seq(p: RecVal)) => Expr(p)
          case invalid => LangException.error(s"single-patt patterns take one child pattern: $invalid")
        }

        val head: Val = v match {
          case ArrVal(Seq(h)) => h
          case invalid => LangException.error(s"single-patt only matches single-element arrays: $invalid")
        }

        Step(
          f = FunVal(Match),
          args = Args.positional(childPatt.rec, head),
          ctx = ctx
        )
      },

      // ARR-PATT //

      patt("arr-patt") { (staticArgs: ArrVal, v: Val, ctx: Ctx) =>

        val patterns: Seq[Val] = staticArgs match {
          case ArrVal(ps) => ps
          case invalid => LangException.error(s"arr-patt patterns take an array of patterns: $invalid")
        }

        val values = v match {
          case ArrVal(vs) => vs
          case invalid => LangException.error(s"arr-patt take an array of values: $invalid")
        }

        if (patterns.length != values.length) {
          LangException.error(s"arr-patt must be given equal numbers of patterns and values: $patterns != $values")
        }

        // Step to evaluate expression i
        def step(i: Int): Step = {
          assert(i < patterns.length)
          Step(
            f = FunVal(Match),
            args = Args.positional(
              patterns(i),
              values(i)
            ),
            ctx = ctx.withK("express", stepk(i))
          )
        }

        // What happens after expression i
        def stepk(i: Int): Fun = {
          if (i + 1 < patterns.length) {
            new Fun {
              override def toString = s"<block:express-$i>"
              def apply(args: Args, ctx: Ctx) = step(i+1)
            }
          } else {
            ctx.k("express").f
          }
        }

        // Either evaluate the first expression or express "unit"
        if (patterns.isEmpty) {
          Step(
            ctx.k("express"),
            Args.positional((StrVal("unit"))),
            ctx
          )
        } else {
          step(0)
        }

      },

      // IF //

      fun("if") { (args: Args, ctx: Ctx) =>

        val (testExpr, trueExpr, falseExpr) = args.extractPositional("Expected condition, true and false expressions") {
          case Seq(c, t, f) => (c, t, f)
        }

        val origCtx = ctx

        val branchK = new Fun {
          override def toString = s"<if:branch>"
          def apply(args: Args, ctx: Ctx) = {
            val testResult = args.extractPositional("Expected true or false values") {
              case Seq(StrVal("true")) => true
              case Seq(StrVal("false")) => false
            }
            Step(
              f = FunVal(Eval),
              args = Args.positional(if (testResult) trueExpr else falseExpr),
              ctx = origCtx
            )
          }
        }

        Step(
          f = FunVal(Eval),
          args = Args.positional(testExpr),
          ctx = ctx.withK("express", branchK)
        )
      },

      // APPLY //

      lam("apply") { (args: Args, ctx: Ctx) =>
        val (f, funArgs) = args.extractPositional("Expected function value and function arguments array") {
          case Seq(f: FunVal, funArgs: ArrVal) => (f, funArgs)
        }

        Step(
          f = f,
          args = Args.positional(funArgs.a: _*),
          ctx = ctx
        )
      },

      // * //
      
      arithOp("*") { (a: Int, b: Int) => a * b },

      // / //
      
      arithOp("/") { (a: Int, b: Int) => a / b },
      
      // + //

      arithOp("+") { (a: Int, b: Int) => a + b },

      // - //
      
      arithOp("-") { (a: Int, b: Int) => a - b },

      // < //

      lam("<") { (args: Args, ctx: Ctx) =>
        val result = args.extractPositional("Expected two integer arguments") {
          case Seq(IntVal(a), IntVal(b)) => if (a < b) StrVal("true") else StrVal("false")
        }
        Step(
          ctx.k("express"),
          Args(RecVal("positional" -> ArrVal(result))),
          ctx
        )
      },

      // PRINTLN //

      lam("println") { (args: Args, ctx: Ctx) =>
        val v: Val = args.extractPositional("Expected single positional argument") {
          case Seq(v) => v
        }
        val s: String = v match {
          case StrVal(s) => s
          case ArrVal(a) => a.mkString("[", ",", "]")
          case IntVal(i) => i.toString
          case RecVal(r) => "<rec>"
          case FunVal(f) => "<fun>"
        }
        external.println(s)
        Step(
          ctx.k("express"),
          Args(RecVal(
            "positional" -> ArrVal(StrVal("unit"))
          )),
          ctx
        )
      }
    ))
  }
}
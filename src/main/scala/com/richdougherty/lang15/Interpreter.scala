package com.richdougherty.lang15

import scala.annotation.tailrec
import scala.collection.mutable

class Interpreter(external: External, logger: Logger) {

  def run(expr: Expr): Val  = {

    val env = Lib.stdEnv(external)

    val ks = Continuations(RecVal(
      "express" -> FunVal(Step.Exit),
      "exit" -> FunVal(Step.Exit)
    ))
    val ctx = Ctx(RecVal(
      "env" -> env.rec,
      "ks" -> ks.rec
    ))

    val exprFun: Fun = env.lookup(expr.name) match {
      case Some(FunVal(f)) => f
      case Some(invalid) => LangException.error(s"Expression evaluator not a function: ${expr.name}: ${invalid}") 
      case None => LangException.error(s"Expression evaluator not defined: ${expr.name}")
    }

    val funArgs: Args = {
      val funArgsRec: RecVal = RecVal()
      funArgsRec += ("positional" -> expr.positional.getOrElse(ArrVal()))
      Args(funArgsRec)
    }

    Step.run(Step(FunVal(exprFun), funArgs, ctx), logger)
  }

}

object Interpreter {

  def run(source: String, external: External = new JVMExternal(), logger: Logger = Logger.Nop): Val = {
    val parser = new Parser()
    val expr = parser.parse(source) match {
      case Some(r: RecVal) => Expr(r)
      case invalid => LangException.error(s"Parse failed: $invalid")
    }
    val interpreter = new Interpreter(external, logger)
    interpreter.run(expr)
  }

}
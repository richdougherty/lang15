package com.richdougherty.lang15

class LangException(message: String) extends Exception(message)

object LangException {
  def error(message: String): Nothing = throw new LangException(message)
}
package com.richdougherty.lang15

import scala.collection.mutable

sealed trait Val
final case class StrVal(s: String) extends Val {
  override def toString = s
}
final case class IntVal(i: Int) extends Val {
  override def toString = i.toString
}
final case class ArrVal(a: mutable.Buffer[Val]) extends Val {
  override def toString = a.mkString("ArrVal(", " ", ")")
}
final case class RecVal(m: mutable.Map[RecKey,Val] = mutable.Map[RecKey,Val]()) extends Val {
  override def toString = {
    (get("name"), get("positional")) match {
      case (Some(StrVal(name)), Some(ArrVal(seq))) if m.size == 2 =>
        s"expr:($name ${seq.mkString(" ")})"
      case _ =>
        m.toString
    }
  }
  def get(k: String): Option[Val] = m.get(StrKey(k))
  def collect[A](name: String)(as: PartialFunction[Option[Val], A]): A = {
    as.applyOrElse(get(name), (got: Option[Val]) => throw new LangException("Couldn't collect '$name': $got"))
  }
  def +=(kv: (String, Val)) = m += (StrKey(kv._1) -> kv._2)
}
final case class FunVal(f: Fun) extends Val {
  override def toString = f.toString
}

object ArrVal {
  def apply(elements: Val*): ArrVal = ArrVal(mutable.Buffer(elements: _*))
}
object RecVal {
  def apply(entries: (String,Val)*): RecVal = {
    val mapped: Seq[(RecKey,Val)] = entries.map { case (k, v) => (StrKey(k),v) }
    val m = mutable.Map(mapped: _*)
    RecVal(m)
  }
}

sealed trait RecKey
final case class StrKey(s: String) extends RecKey

package com.richdougherty.lang15

trait External {
  def println(s: String): Unit
}

class JVMExternal extends External {
  def println(s: String): Unit = System.out.println(s)
}
package com.richdougherty.lang15

trait Logger {
  def log(s: => String): Unit
}

object Logger {
  object Nop extends Logger {
    def log(s: => String) = ()
  }
}
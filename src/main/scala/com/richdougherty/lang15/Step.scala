package com.richdougherty.lang15

import scala.annotation.tailrec

final case class Step(f: FunVal, args: Args, ctx: Ctx)

object Step {

  object Exit extends Fun {
    override def toString = "<exit>"
    def apply(args: Args, ctx: Ctx): Step = ???
  }

  @tailrec
  def run(step: Step, logger: Logger = Logger.Nop): Val = step match {
    case Step(FunVal(Exit), args, _) => args.rec
    case Step(FunVal(f), args, ctx) => {
      logger.log(s"Running: ${step.f.f} ${args.positional}")
      run(f(args, ctx), logger = logger)
    }
  }
}

trait Fun {
  def apply(args: Args, ctx: Ctx): Step
}

final case class Ctx(rec: RecVal) {
  val env: Env = Env(rec.collect("env") {
    case Some(r: RecVal) => r
  })
  val ks: Continuations = Continuations(rec.collect("ks") {
    case Some(r: RecVal) => r
  })
  def withChildEnv: Ctx = {
    Ctx(RecVal(
      "env" -> {
        val childEnv = RecVal()
        childEnv += ("<parent>" -> env.rec)
        childEnv
      },
      "ks" -> ks.rec
    ))
  }
  def k(name: String) = ks.rec.collect(name) { case Some(f: FunVal) => f }
  def withK(name: String, k: Fun): Ctx = {
    Ctx(RecVal(
      "env" -> env.rec,
      "ks" -> {
        val ksCopy = RecVal()
        ksCopy.m ++= ks.rec.m
        ksCopy += (name -> FunVal(k))
        ksCopy
      }
    ))
  }
}

final case class Env(rec: RecVal) {
  def lookup(name: String): Option[Val] = {
    @tailrec def lookupRec(rec: RecVal): Option[Val] = {
      val scopeResult = rec.get(name)
      if (scopeResult.isDefined) scopeResult else {
        rec.get("<parent>") match {
          case Some(parentRec: RecVal) => lookupRec(parentRec)
          case None => None
          case Some(invalid) => LangException.error("Lexical env parent must be a record: $invalid")
        }
      }
    }
    lookupRec(rec)
  }
}
final case class Continuations(rec: RecVal)

final case class Expr(rec: RecVal) {
  val name: String = rec.collect("name") { case Some(StrVal(s)) => s }
  val positional: Option[ArrVal] = rec.collect("positional") {
    case Some(arr: ArrVal) => Some(arr)
    case None => None
  }
}

final case class Args(rec: RecVal) {
  val positional: ArrVal = rec.get("positional") match {
    case Some(a: ArrVal) => a
    case invalid => LangException.error(s"Invalid positional args: $invalid")
  }
  def extractPositional[A](expectedMsg: String)(extractor: PartialFunction[Seq[Val],A]): A = {
    val argSeq = positional.a
    extractor.applyOrElse(argSeq, (_: Seq[Val]) => LangException.error("Invalid args $argSeq: $expectedMsg"))
  }
}
object Args {
  def positional(vs: Val*) = {
    val rec: RecVal = RecVal()
    rec += ("positional" -> ArrVal(vs: _*))
    Args(rec)
  }
}

package com.richdougherty.lang15

import org.parboiled.scala._
import org.parboiled.errors.ErrorUtils
import org.scalatest._
import scala.collection.mutable

class LibSpec extends UnitSpec {
  import LibSpec._

  def run(source: String, log: Boolean = false): Either[Val,Seq[Val]] = {
    val logger = if (log) new SpecLogger() else Logger.Nop
    val v = Interpreter.run(source, logger = logger)
    v match {
      case r: RecVal => r.get("positional") match {
        case Some(ArrVal(a)) => Right(a.to[Seq])
        case invalid => Left(v)
      }
      case invalid => Left(v)
    }
  }

  def runMocked(source: String, mockExternal: External): Either[Val,Seq[Val]] = {
    val v = Interpreter.run(source, external = mockExternal) 
    v match {
      case r: RecVal => r.get("positional") match {
        case Some(ArrVal(a)) => Right(a.to[Seq])
        case invalid => Left(v)
      }
      case invalid => Left(v)
    }
  }

  "The interpreter" when {
    "running `str` expressions" should {
      "return the str value xyz" in {
        run("(str xyz)") should be(Right(Seq(StrVal("xyz"))))
      }
      "return the str value \"\"" in {
        run("(str \"\")") should be(Right(Seq(StrVal(""))))
      }
      "return the str value 12" in {
        run("(str 12)") should be(Right(Seq(StrVal("12"))))
      }
      "return the str value -1" in {
        run("(str -1)") should be(Right(Seq(StrVal("-1"))))
      }
    }
    "running `int` expressions" should {
      "return the int value 12" in {
        run("(int 12)") should be(Right(Seq(IntVal(12))))
      }
      "return the int value -1" in {
        run("(int -1)") should be(Right(Seq(IntVal(-1))))
      }
    }
    "running `arr` expressions" should {
      "return empty arrays" in {
        run("(arr)") should be(Right(Seq(ArrVal())))
      }
      "return arrays with one string value" in {
        run("(arr (str x))") should be(Right(Seq(ArrVal(StrVal("x")))))
      }
      "return arrays with mixed values" in {
        run("(arr (int 0) (int 1) (str end))") should be(Right(Seq(ArrVal(
          IntVal(0), IntVal(1), StrVal("end")
        ))))
      }
    }
    "running `eval` expressions" should {
      "evaluate one `int` expression" in {
        run("(eval (int 1))") should be(Right(Seq(IntVal(1))))
      }
      "evaluate two `int` expressions" in {
        run("(eval (int 1) (int 2))") should be(Right(Seq(IntVal(1), IntVal(2))))
      }
    }
    "running `quote` expressions" should {
      "quote an `int` expression" in {
        run("(quote (int 1))") should be(Right(Seq(RecVal(
          "name" -> StrVal("int"),
          "positional" -> ArrVal(StrVal("1"))
        ))))
      }
      "quote a pair of `int` expressions" in {
        run("(arr (quote (int 1)) (quote (int 2)))") should be(Right(Seq(ArrVal(
          RecVal(
            "name" -> StrVal("int"),
            "positional" -> ArrVal(StrVal("1"))
          ),
          RecVal(
            "name" -> StrVal("int"),
            "positional" -> ArrVal(StrVal("2"))
          )
        ))))
      }
    }
    "running `apply` expressions" should {
      "apply + to to args" in {
        run("(apply (lookup +) (arr (quote (int 1)) (quote (int 2))))") should be(Right(Seq(IntVal(3))))
      }
    }
    "running `block` expressions" should {
      "handle empty block expressions" in {
        run("(block)") should be(Right(Seq(StrVal("unit"))))
      }
      "evaluate one `int` expression" in {
        run("(block (int 1))") should be(Right(Seq(IntVal(1))))
      }
      "evaluate two `int` expressions, expressing the last one" in {
        run("(block (int 1) (int 2))") should be(Right(Seq(IntVal(2))))
      }
      "preserve values in the old scope" in {
        run("""
          (block
            (let (single-patt (bind-patt x)) (int 1))
            (block
              (let (bind-patt x) (int 2)))
            (lookup x))""") should be(Right(Seq(IntVal(1))))
      }
      "introduce values in a new scope" in {
        run("""
          (block
            (let (single-patt (bind-patt x)) (int 1))
            (block
              (let (single-patt (bind-patt x)) (int 2))
              (lookup x)))""") should be(Right(Seq(IntVal(2))))
      }
    }
    "running `lookup` expressions" should {
      "look up `eval`" in {
        run("(lookup eval)") should be(Right(Seq(FunVal(Lib.Eval))))
      }
    }
    "running 'match' expressions" should {
      "bind a passed stirng value with bind-patt" in {
        run("""
          (block
            (match (bind-patt x) value)
            (lookup x))""") should be(Right(Seq(StrVal("value"))))
      }
      "bind a passed array value with bind-patt" in {
        run("""
          (block
            (apply (lookup match) (arr (quote (bind-patt x)) (arr (int 1) (int 2))))
            (lookup x))""") should be(Right(Seq(ArrVal(IntVal(1), IntVal(2)))))
      }
      "match single-element arrays with single-patt" in {
        run("""
          (block
            (apply (lookup match) (arr (quote (single-patt (bind-patt x))) (arr (int 1))))
            (lookup x))""") should be(Right(Seq(IntVal(1))))
      }
      "match arrays with arr-patt and lookup first element" in {
        run("""
          (block
            (apply
              (lookup match)
              (arr
                (quote (arr-patt (bind-patt x) (bind-patt y)))
                (arr (int 1) (int 2))))
            (lookup x))""") should be(Right(Seq(IntVal(1))))
      }
      "match arrays with arr-patt and lookup second element" in {
        run("""
          (block
            (apply
              (lookup match)
              (arr
                (quote (arr-patt (bind-patt x) (bind-patt y)))
                (arr (int 1) (int 2))))
            (lookup x))""") should be(Right(Seq(IntVal(1))))
      }
    }
    "running `let` expressions" should {
      "let `x` be 1" in {
        run("(let (single-patt (bind-patt x)) (int 1))") should be(Right(Seq(StrVal("unit"))))
      }
      "get `x` back after letting it be 1" in {
        run("(block (let (single-patt (bind-patt x)) (int 1)) (lookup x))") should be(Right(Seq(IntVal(1))))
      }
    }
    "running `lambda` expressions" should {
      "be callable with one argument" in {
        run("""
          (block
            (let (single-patt (bind-patt double)) (lambda (single-patt (bind-patt x)) (+ (lookup x) (lookup x))))
            (double (int 2)))""") should be(Right(Seq(IntVal(4))))
      }
      "be callable by apply with one argument" in {
        run("""
            (apply (lambda (single-patt (bind-patt x)) (+ (lookup x) (lookup x))) (arr (quote (int 2))))
            """) should be(Right(Seq(IntVal(4))))
      }
      "be callable with two argument" in {
        run("""
          (block
            (let (single-patt (bind-patt add)) (lambda (arr-patt (bind-patt x) (bind-patt y)) (+ (lookup x) (lookup y))))
            (add (int 5) (int 2)))""") should be(Right(Seq(IntVal(7))))
      }
      "be callable with three argument" in {
        run("""
          (block
            (let (single-patt (bind-patt add-mul))
              (lambda (arr-patt (bind-patt x) (bind-patt y) (bind-patt z))
                (* (+ (lookup x) (lookup y)) (lookup z))))
            (add-mul (int 5) (int 2) (int 3)))""") should be(Right(Seq(IntVal(21))))
      }
    }
    "running 'vau' expressions" should {
      "receive its args unevaluated" in {
        run("""
            (apply (vau (bind-patt args) (bind-patt env) (lookup args)) (arr (quote (int 1)) (quote (int 2))))
            """) should be(Right(Seq(ArrVal(
              RecVal(
                "name" -> StrVal("int"),
                "positional" -> ArrVal(StrVal("1"))
              ),
              RecVal(
                "name" -> StrVal("int"),
                "positional" -> ArrVal(StrVal("2"))
              )
            ))))
      }
    }
    "running 'if' expressions" should {
      "evaluate the first branch when true" in {
        run("(if (str true) (int 1) (int 2))") should be(Right(Seq(IntVal(1))))
      }
      "evaluate the second branch when false" in {
        run("(if (str true) (int 1) (int 2))") should be(Right(Seq(IntVal(1))))
      }
    }
    "running '<' expressions" should {
      "compare 1<2" in {
        run("(< (int 1) (int 2))") should be(Right(Seq(StrVal("true"))))
      }
      "compare 1<1" in {
        run("(< (int 1) (int 1))") should be(Right(Seq(StrVal("false"))))
      }
      "compare 1<0" in {
        run("(< (int 1) (int 0))") should be(Right(Seq(StrVal("false"))))
      }
    }
    "running '*' expressions" should {
      "multiply 2*6" in {
        run("(* (int 2) (int 6))") should be(Right(Seq(IntVal(12))))
      }
    }
    "running '/' expressions" should {
      "divide 10/5" in {
        run("(/ (int 10) (int 5))") should be(Right(Seq(IntVal(2))))
      }
    }
    "running '+' expressions" should {
      "add 1+2" in {
        run("(+ (int 1) (int 2))") should be(Right(Seq(IntVal(3))))
      }
    }
    "running '-' expressions" should {
      "subtract 1-2" in {
        run("(- (int 1) (int 2))") should be(Right(Seq(IntVal(-1))))
      }
    }
    "running 'println' expressions" should {
      "print string values" in {
        val mockExternal = new MockExternal()
        runMocked("(println (str xyz))", mockExternal) should be(Right(Seq(StrVal("unit"))))
        mockExternal.calls should be(Seq(Println("xyz")))
      }
    }
    "running programs" should {
      "run a (factorial 4) mini-program" in {
        run("""
          (block
            (let (single-patt (bind-patt factorial))
              (lambda
                (single-patt (bind-patt n))
                (block
                  (if (< (lookup n) (int 1))
                    (int 1)
                    (* (lookup n) (factorial (- (lookup n) (int 1))))))))
            (factorial (int 4)))""") should be(Right(Seq(IntVal(24))))
      }
    }
  }

}

object LibSpec {

  class MockExternal extends External {
    var calls: Seq[Call] = Seq()
    private def call(c: Call): Unit = {
      calls = (calls :+ c)
    }
    def println(s: String): Unit = call(Println(s))
  }

  sealed trait Call
  final case class Println(s: String) extends Call

}
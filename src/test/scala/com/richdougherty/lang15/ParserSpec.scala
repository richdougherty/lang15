package com.richdougherty.lang15

import org.parboiled.scala._
import org.parboiled.errors.ErrorUtils
import org.scalatest._

class ParserSpec extends UnitSpec {

  val parser = new Parser() {
    override val buildParseTree = true
  }

  def parse[A](rule: Parser => Rule1[A], input: String) = {
    val runner = ReportingParseRunner(rule(parser))
    val parsingResult = runner.run(input)
    //println(org.parboiled.support.ParseTreeUtils.printNodeTree(parsingResult))
    ErrorUtils.printParseErrors(parsingResult)
    parsingResult.result
  }

  "The parser" when {
    "parsing nonsense" should {
      "parse 'a'" in {
        parse(_.program, "•••") should be(None)
      }
    }
    "parsing strings" should {
      "parse 'a'" in {
        parse(_.str, "a") should be(Some(StrVal("a")))
      }
      "parse '+'" in {
        parse(_.str, "+") should be(Some(StrVal("+")))
      }
      "parse 'abc'" in {
        parse(_.str, "abc") should be(Some(StrVal("abc")))
      }
      "parse 0" in {
        parse(_.str, "0") should be(Some(StrVal("0")))
      }
      "parse 1" in {
        parse(_.str, "1") should be(Some(StrVal("1")))
      }
      "parse 99" in {
        parse(_.str, "99") should be(Some(StrVal("99")))
      }
      "parse -1" in {
        parse(_.str, "-1") should be(Some(StrVal("-1")))
      }
      "parse -12345" in {
        parse(_.str, "-12345") should be(Some(StrVal("-12345")))
      }
    }
    "parsing double-quoted strings" should {
      "parse ''" in {
        parse(_.dqstr, "\"\"") should be(Some(StrVal("")))
      }
      "parse 'a'" in {
        parse(_.dqstr, "\"a\"") should be(Some(StrVal("a")))
      }
      "parse 'x y z'" in {
        parse(_.dqstr, "\"x y z\"") should be(Some(StrVal("x y z")))
      }
    }
    "parsing expressions" should {
      "parse 'a'" in {
        parse(_.expr, "a") should be(Some(StrVal("a")))
      }
      "parse 'abc'" in {
        parse(_.expr, "abc") should be(Some(StrVal("abc")))
      }
      "parse '(a)'" in {
        parse(_.expr, "(a)") should be(Some(RecVal(
          "name" -> StrVal("a"),
          "positional" -> ArrVal()
        )))
      }
      "parse '(a b)'" in {
        parse(_.expr, "(a b)") should be(Some(RecVal(
          "name" -> StrVal("a"),
          "positional" -> ArrVal(StrVal("b"))
        )))
      }
      "parse '(a\\nb)'" in {
        parse(_.expr, "(a\nb)") should be(Some(RecVal(
          "name" -> StrVal("a"),
          "positional" -> ArrVal(StrVal("b"))
        )))
      }
      "parse '(a b c)'" in {
        parse(_.expr, "(a b c)") should be(Some(RecVal(
          "name" -> StrVal("a"),
          "positional" -> ArrVal(StrVal("b"), StrVal("c"))
        )))
      }
      "parse '(a (b c))'" in {
        parse(_.expr, "(a (b c))") should be(Some(RecVal(
          "name" -> StrVal("a"),
          "positional" -> ArrVal(RecVal(
            "name" -> StrVal("b"),
            "positional" -> ArrVal(StrVal("c"))
          ))
        )))
      }
    }
    "parsing programs" should {
      "parse ' a'" in {
        parse(_.program, " a") should be(Some(StrVal("a")))
      }
      "parse '  a'" in {
        parse(_.program, "  a") should be(Some(StrVal("a")))
      }
      "parse 'a '" in {
        parse(_.program, "a ") should be(Some(StrVal("a")))
      }
      "parse ' (a)'" in {
        parse(_.program, " (a)") should be(Some(RecVal(
          "name" -> StrVal("a"),
          "positional" -> ArrVal()
        )))
      }
      "parse ' (a b) '" in {
        parse(_.program, " (a b) ") should be(Some(RecVal(
          "name" -> StrVal("a"),
          "positional" -> ArrVal(StrVal("b"))
        )))
      }
      "parse '(a\\n  (b c))'" in {
        parse(_.expr, "(a\n  (b c))") should be(Some(RecVal(
          "name" -> StrVal("a"),
          "positional" -> ArrVal(RecVal(
            "name" -> StrVal("b"),
            "positional" -> ArrVal(StrVal("c"))
          ))
        )))
      }
      "parse '(+ (int 1) (int 2))'" in {
        parse(_.expr, "(+ (int 1) (int 2))") should be(Some(RecVal(
          "name" -> StrVal("+"),
          "positional" -> ArrVal(RecVal(
            "name" -> StrVal("int"),
            "positional" -> ArrVal(StrVal("1"))
          ),
          RecVal(
            "name" -> StrVal("int"),
            "positional" -> ArrVal(StrVal("2"))
          ))
        )))
      }
      "parse multiline expressions" in {
        parse(_.program, """(block
            (let x 1)
            (let y 1)
            (block
              (let x 2)
              (+ (lookup x) (lookup y))))""") should be ('defined)
      }
      "parse multiline expressions that break after the first argument" in {
        parse(_.program, """
            (lambda
                (single-patt (bind-patt n))
                (block
                  (if (< (lookup n) (int 1))
                    (int 1)
                    (* (lookup n) (factorial (- (lookup n) (int 1)))))))
            """) should be ('defined)
      }
    }
  }

}